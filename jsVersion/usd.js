const http = require("https")
const cheerio = require("cheerio")

/*
    chrome version
    fetch(`https://www.hncb.com.tw/hncb/rest/exRate/immTrend/?cur=USD&_=${t}`)
    .then(r => {
        return r.json()
    }).then(result => {
        console.log(result[result.length - 1])
    })
*/

let target = 29.9;
//let time = 600   // 單位秒
t = Date.now()

function infoIFTTT(bankName, price) {
    http.get(`https://maker.ifttt.com/trigger/USD/with/key/cLdCMBob353ofTBdwNuujZ?value1=${bankName}&value2=${price}`)
}

function getFromHNCB() {
    http.get(`https://www.hncb.com.tw/hncb/rest/exRate/immTrend/?cur=USD&_=${t}`, res => {
        let data = "";

        res.on('data', d => {
            data += d
        })

        res.on("end", () => {
            result = JSON.parse(data)
            price = parseFloat(result[result.length - 1].SELL_AMT_BOARD)
            console.log(`HNCB: ${price}`)

            if (price < target) {
                // make sound
                console.log("\007");
                infoIFTTT("hncb", price)
            }
        })

    })
}

function getFromSC() {
    http.get("https://service.standardchartered.com.tw/check/inquiry-rate-foreign-exchange.asp", res => {
        let html = ""

        res.on('data', data => {
            html += data
        })

        res.on("end", () => {
            let $ = cheerio.load(html)
            let price_s = $("#innertable > table > tbody > tr:nth-child(2) > td:nth-child(5)").text()
            let price = parseFloat(price_s)
            console.log(`SC: ${price}`)

            // make sound
            if (price < target) {
                console.log("\007");
                infoIFTTT("sc", price)
            }
        })
    })
}

/* setInterval(() => {

    console.log("=================")
    getFromHNCB()
    getFromSC()

}, time * 1000) */

getFromHNCB()
getFromSC()

