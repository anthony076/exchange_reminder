﻿using System;
using System.IO;            // 用於讀取數據流
using System.Net;           // 用於發起和接收請求
using Newtonsoft.Json.Linq; // 序列化 JSON 用

namespace csVersion
{
    class Program
    {
        static void hncb()
        {
            // ==== send GET request ====
            string result = "";

            // get timestamp
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);    // 產生timestamp
            string t_s = Convert.ToInt64(t.TotalSeconds).ToString();                // 將 timestamp 轉換為字串

            string targetUrl = String.Format("https://www.hncb.com.tw/hncb/rest/exRate/immTrend/?cur=USD&_={0}", t_s);

            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            /*
            // 設定 request header，非必要
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 3000; */

            // ==== get GET response ====
            // using ，相當於 python 的 with，會自動執行執行 response.close()
            // step1，透過 request.GetResponse() 取得 response instance
            // step2，透過 response.GetResponseStream() 讀取 byte 數據
            // step3，透過 StreamReader 轉換為 string
            /*
            // 使用 using 的寫法
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    // 取的 json-string
                    result = sr.ReadToEnd();

                    // 使用 using 不需要手動調用 close()
                    // response.close()
                }
            } */
            // 不使用 using 的寫法
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            result = sr.ReadToEnd();
            response.Close();

            // 將 json-string 的 response 進行反序列化
            // json-string 反序列，返回 Jarray 類型 Object
            dynamic json = JValue.Parse(result);
            // 取最後一個元素中的 SELL_AMT_BOARD key 的值
            string price = (string)json[json.Count - 1].SELL_AMT_BOARD;

            Console.WriteLine(price);
        }

        static void sc()
        {

        }

        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            hncb();
        }
    }
}
